**Adriane Aytona**		**26 Jan**

**1.) Relative Layout**
- I am a complete beginner to any mobile application development so
I started looking up simple tutorials for Android Studio. Since we
are required to create a mechanism to show some basic info about
the app. From the tutorials I found on YouTube, I realized I wanted 
to put the button (my mechanism) on the bottom center of my screen. 
immediately looked for the proper xml code to do that; however 
the default layout - Constraint Layout - does not have the needed 
code for that. At least I did not find one.
So I just looked up another layout that has one and I found 
Relative Layout to use.
	
**2.) Picasso invokation**
- The image I used for this project is a meme I found on twitter. 
Judging by its size, I thought it would not fit properly on any 
phone screen. Instead of using the settings we used during the 
last meeting, I went to the main page of Picasso and looked for 
any helpful information. I used the few lines of code they have 
under IMAGE TRANSFORMATION (http://square.github.io/picasso/). 
However the app shows a blurry version of the image. In the end, I 
chose to use the settings we used in class. 

**23 Fri**

**1.) Straight from lectures**
- To be fairly honest, I based everything I did for this assignment 
from the lectures we had a couple of weeks ago. I did not find the 
need to drastically change things to implement them in my MemeApp 
because the lectures about Intent and etc. were easy to use. 
However, I only spent a good amount of time looking back at them 
because the lectures were a long time ago. Consequently, I have to 
make small changes to make the code from the lectures viable for 
my assignment.

**2.) Still Relative Layout**
- For the new things to be added for this assignment, I still
sticked to using Relative Layout primarily because I am fully 
familiar to the layout elements needed to place the new UI 
elements. Fortunately I only encountered simple problems 
(i.e. positioning) while working on this assignment. All in all, 
this layout works perfectly for my version of this assignment.