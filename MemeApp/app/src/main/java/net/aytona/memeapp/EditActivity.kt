package net.aytona.memeapp

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_edit.*

class EditActivity : AppCompatActivity() {

    companion object {

        val imgUrl = "URL"
        val topText = "TOP"
        val btmText = "BTM"

        fun createIntent(context: Context, meme: Meme) : Intent {
            val intent = Intent(context, EditActivity::class.java)
            intent.putExtra("URL", meme.imgURL)
            intent.putExtra("TOP", meme.topText)
            intent.putExtra("BTM", meme.btmText)
            return intent
        }

        fun readURLIntent(intent: Intent) : String {
            return intent.getStringExtra(imgUrl)
        }

        fun readtopIntent(intent: Intent) : String {
            return intent.getStringExtra(topText)
        }

        fun readbtmIntent(intent: Intent) : String {
            return intent.getStringExtra(btmText)
        }
    }

    var memeUrl : String? = null
    var memeTop : String? = null
    var memeBtm : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)

        memeUrl = EditActivity.readURLIntent(intent)
        imgUrlBox.setText(memeUrl.toString())
        memeTop = EditActivity.readtopIntent(intent)
        toptextBox.setText(memeTop.toString())
        memeBtm = EditActivity.readbtmIntent(intent)
        btmtextBox.setText((memeBtm.toString()))

        btnFinish.setOnClickListener{
            val intent = Intent()
            intent.putExtra("URL", imgUrlBox.getText().toString())
            intent.putExtra("TOP", toptextBox.getText().toString())
            intent.putExtra("BTM", btmtextBox.getText().toString())
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }
}
