package net.aytona.memeapp

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.widget.Toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var meme: Meme
    val editRequest = 0xBEEF
    lateinit var url: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Instance stuff
        if(savedInstanceState == null) {
            meme = Meme("https://pbs.twimg.com/media/DT1MtFZU8AAKW-8.jpg",
                    "Hey Ohaiyo! EE here!", "PIZZA Time!")
        }

        Log.d("MainActivity", "onCreate")
        setContentView(R.layout.activity_main)

        // About button (mechanism)
        btnShowInfo.setOnClickListener({
            val alertDialog = AlertDialog.Builder(this).create()
            alertDialog.setTitle("App Info")
            alertDialog.setMessage("Name: MemeApp\nAuthor: Adriane Aytona\nVersion 2.0")
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", {
                dialogInterface, i -> Toast.makeText(applicationContext, "You read the info!",
                    Toast.LENGTH_SHORT).show()
            })
            alertDialog.show()
        })

        // draw the meme with image, top text, and bottom text
        drawImageMeme(meme.imgURL, meme.topText, meme.btmText)

        // listener for edit meme button
        btnEdit.setOnClickListener {
            editMeme()
        }
    }

    fun editMeme() {            // calls EditActivity
        startActivityForResult(EditActivity.createIntent(this, meme), editRequest)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == editRequest) {
            if(resultCode == Activity.RESULT_OK) {
                meme.imgURL = data!!.getStringExtra("URL")
                meme.topText = data!!.getStringExtra("TOP")
                meme.btmText = data!!.getStringExtra("BTM")

                drawImageMeme(meme.imgURL, meme.topText, meme.btmText)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putString("url", meme.imgURL)
        outState?.putString("top", meme.topText)
        outState?.putString("btm", meme.btmText)
    }

    fun drawImageMeme(source: String, top: String, btm: String) {
        Picasso.with(this)
                .load(source)
                .into(imgMeme)
        topText.setText(top)
        btmText.setText(btm)
    }
}

data class Meme (
        var imgURL: String,
        var topText: String,
        var btmText: String
)
